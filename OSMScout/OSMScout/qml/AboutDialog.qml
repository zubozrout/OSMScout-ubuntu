import QtQuick 2.4
import QtQuick.Layouts 1.1
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3
import net.sf.libosmscout.map 1.0

import "custom"

Page {
	id: aboutPage
	anchors.fill: parent
    visible: false
	header: PageHeader {
		id: header
		title: qsTr("About OSMScout")
		flickable: aboutFlickable
		
		leadingActionBar {
			actions: [
				Action {
					iconName: "back"
					text: "Close"
					onTriggered: {
						map.reopenMap();
						aboutPage.visible = false;
						aboutPage.closed();
					}
				}
			]
		numberOfSlots: 1
		}
	}
	
	signal closed();
	
	Flickable {
		id: aboutFlickable
		anchors.fill: parent
		contentWidth: parent.width
		contentHeight: dialogue.height
		flickableDirection: Flickable.VerticalFlick
		
		Column {
			id: dialogue
			anchors {
				fill: parent
				margins: units.gu(2)
			}
			spacing: units.gu(2)

			Label {
				width: parent.width
				wrapMode: Text.Wrap
				text: qsTr("Ubuntu Touch version of OSMScout")+"<br/>"+qsTr("See")+" <br/>" + url + "<br/>"+qsTr("Ubuntu modifications by") + " Schreuder Electronics"
				
				property var url: "https://github.com/fransschreuder/libosmscout"
				
				MouseArea {
					anchors.fill: parent
					onClicked: {
						Qt.openUrlExternally(parent.url);
					}
				}
			}

			Label {
				width: parent.width
				wrapMode: Text.Wrap
				text: "Slight modifications and Xenial build by Martin Kozub: " + url
				
				property var url: "https://gitlab.com/zubozrout/OSMScout-ubuntu"
				
				MouseArea {
					anchors.fill: parent
					onClicked: {
						Qt.openUrlExternally(parent.url);
					}
				}
			}
			
			Label {
				width: parent.width
				wrapMode: Text.Wrap
				text: qsTr("All geographic data:")+"<br/>© OpenStreetMap contributors<br/>See :" + url
				
				property var url: "https://www.openstreetmap.org/copyright"
				
				MouseArea {
					anchors.fill: parent
					onClicked: {
						Qt.openUrlExternally(parent.url);
					}
				}
			}
		}
	}
}
