import QtQuick 2.4
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3

import net.sf.libosmscout.map 1.0
import Qt.labs.settings 1.0

Page {
	id: settingsPage
	anchors.fill: parent
    visible: false
	header: PageHeader {
		id: header
		title: qsTr("Settings")
		flickable: dialogue
		
		leadingActionBar {
			actions: [
				Action {
					iconName: "back"
					text: "Close"
					onTriggered: {
						map.reopenMap();
						settingsPage.visible = false;
						settingsPage.closed();
					}
				}
			]
		numberOfSlots: 1
		}
	}
	
	signal closed();
	
	Settings {
		id: settings
		property bool metricSystem: (Qt.locale().measurementSystem === Locale.MetricSystem)
		//property bool drivingDirUp: false
		property double fontSize: 1
		property int defaultDownloadFolder: (downloadDirListModel.count - 1)
	}
	
	DownloadDirListModel{
		id: downloadDirListModel
	}
	
    Flickable {
        id: dialogue
        anchors.fill: parent
		contentWidth: parent.width
		contentHeight: settingsColumn.height
		flickableDirection: Flickable.VerticalFlick
        
		Column {
			id: settingsColumn
			anchors {
				fill: parent
				margins: units.gu(2)
			}
			spacing: units.gu(2)
			
			Text {
				text: qsTr("Use metric system")
			}
			
			Switch {
				id: swMetric
				checked: settings.metricSystem
				onClicked: {
					settings.metricSystem = checked;
				}
			}

			Text {
				text: qsTr("Font size")
			}
			
			Slider {
				id: slFontSize
				minimumValue: 1
				maximumValue: 10
				value: Math.round(settings.fontSize * 10)
				width: parent.width
				stepSize: 1.0
				onValueChanged: {
					settings.fontSize = value / 10;
				}
				
				function formatValue(v) {
					return v.toFixed(1);
				}
			}
			
			Text {
				text: qsTr("Default download directory")
			}

			OptionSelector {
				anchors {
					left: parent.left
					right: parent.right
				}
				delegate: Component {
					id: selectorDelegate
					OptionSelectorDelegate {
						text: model.path
					}
				}
				model: downloadDirListModel
				onDelegateClicked: {
					settings.defaultDownloadFolder = index;
					console.log("Index changed: "+index);
				}
				selectedIndex: settings.defaultDownloadFolder
			}
		}
    }
}
