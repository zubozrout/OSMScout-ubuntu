import QtQuick 2.4
import QtQuick.Layouts 1.1
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3

import net.sf.libosmscout.map 1.0
import Qt.labs.settings 1.0

import "custom"

Page {
	id: favouritesPage
	anchors.fill: parent
    visible: false
	header: PageHeader {
		id: header
		title: qsTr("Favourites")
		flickable: favouritesFlickable
		
		leadingActionBar {
			actions: [
				Action {
					iconName: "back"
					text: "Close"
					onTriggered: {
						map.reopenMap();
						favouritesPage.visible = false;
						favouritesPage.closed();
					}
				}
			]
		numberOfSlots: 1
		}
	}
	
	signal closed();
    signal opened();
    
    onOpened: {
		loadFavourites();
	}
    
    function saveFavourites() {
		var i;
		favSettings.favs="";
		for(i=0; i<favModel.count; i++)
		{
			favSettings.favs=favSettings.favs+favModel.get(i).title+";"
		}
	}

	function loadFavourites() {
		var favs=favSettings.favs.split(";");
		favModel.clear();
		console.log("Loading favourites...");
		for(var i=0; i<favs.length; i++)
		{
			if(favs[i]!=="")
			{
				console.log("Appending: "+favs[i]);
				favModel.append({'title':favs[i]});
			}
		}
	}
	
	Settings {
		id: favSettings
		category: "favourites"
		property string favs: ""
		property string home: ""
	}
	
	Flickable {
		id: favouritesFlickable
		anchors.fill: parent
		contentWidth: parent.width
		contentHeight: dialogue.height
	
		Column {
			id: dialogue
			anchors {
				fill: parent
				margins: units.gu(2)
			}
			spacing: units.gu(2)

			ListModel {
				id: favModel
			}

			LocationEdit {
				id: locationInput
				text: qsTr("<current position>")
				width: targetInput.width
				height: units.gu(4)
				horizontalAlignment: TextInput.AlignLeft
			}

			Button {
				id: addFav
				text: qsTr("Add to Favourites")
				onClicked: {
					var item = {'title': locationInput.text};
					favModel.append(item);
					saveFavourites();
				}
			}
			
			Label {
				text: qsTr("Favourites list:")
				visible: favModel.count > 0
			}
			
			ListView {
				anchors {
					left: parent.left
					right: parent.right
				}
				id: favSelector
				model: favModel
				height: units.gu(16)
				interactive: true
				visible: favModel.count > 0
				
				delegate: ListItemWithActions {
					height: units.gu(4)
					width: parent.width
					color: index % 2 === 0 ? "transparent" : "#efefef"

					leftSideAction: Action {
						iconName: "delete"
						text: i18n.tr("Delete")
						onTriggered: {
							favModel.remove(index);
							favouritesPage.saveFavourites();
						}
					}

					contents: Label {
						text: title
						anchors.left: parent.left
						anchors.verticalCenter: parent.verticalCenter
					}
				}
			}
			
			Button {
				id: saveHome;
				text: qsTr("Save as home")
				onClicked: {
					favSettings.home = locationInput.text;
				}
			}

			Row {
				width: parent.width
				
				Icon {
					id: homeIcon
					name: "home"
					width: units.gu(4)
				}
				
				Label {
					anchors.verticalCenter: homeIcon.verticalCenter
					text: favSettings.home
				}
			}
		}
	}
}
